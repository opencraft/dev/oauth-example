from django.shortcuts import render

from profiles.oauth import oauth


def login(request):
    # build a full authorize callback uri
    redirect_uri = request.build_absolute_uri('/authorize/')
    return oauth.lms.authorize_redirect(request, redirect_uri)


def authorize(request):
    # This will create a http request client that points to the LMS.
    lms = oauth.create_client('lms')
    # Here, we authenticate the client with the token we got from the LMS. In a real-world
    # application, we'd save this token somehow for subsequent requests.
    token = lms.authorize_access_token(request)
    # And then, we use this token to fetch the user's info.
    response = lms.get('/api/user/v1/me', token=token)
    response.raise_for_status()
    profile = response.json()
    # Now that we have the user's info, we can render a page with the relevant info.
    return render(request, 'authorize.html', {'profile': profile})
