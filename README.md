# About this Project

This project is a companion repository for the blog post by Fox Piacenti on
[how to use the Open edX platform for authentication](https://opencraft.com/blog/use-the-open-edx-lms-for-authentication/)

It contains the complete code used in the post. Read there to learn more about it.
